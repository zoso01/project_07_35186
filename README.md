# Milionerzy

![](https://blog.zabawkitotu.pl/wp-content/uploads/2019/09/milionerzy-gra-planszowa-tmtoys-zabawkitotu-pl-850x550.jpg)

------------
### Instrukcja obsługi dla użytkownika:
1. Grę można rozpocząć po naciśnięciu klawisza Enter.
2. Po pojawieniu się pytania użytkownik może udzielić odpowiedzi na pytanie, wpisując literę odpowiadającą danemu wariantowi odpowiedzi (A-B-C-D). Wielkość podanej litery nie ma znaczenia. 
3. Jeżeli użytkownik nie zna odpowiedzi, może skorzystać z kół ratunkowych - pół na pół (wpisując "50/50" lub "50:50"), pytania do publiczności (wpisując "publicznosc") lub telefonu do przyjaciela (wpisując "przyjaciel").
4. Po udzieleniu poprawnej odpowiedzi należy wcisnąć klawisz Enter, aby kontynuować grę. 
5. Po udzieleniu złej odpowiedzi lub pomyślnym odpowiedzeniu na wszystkie 15 pytań z gry można wyjść po naciśnięciu dowolnego klawisza.


### Narzędzia wykorzystane do stworzenia gry:
- Qt Creator
- Visual Studio Code
- Microsoft Office Excel 
### Dokumentacja:
![](https://i.imgur.com/skueSuN.png)
