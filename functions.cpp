#include "functions.h"

using namespace std;

int Draw(const int min, const int max){
    srand(time(NULL));
    return rand() % max + min;
}

void Question::questionData(string fileName) {
    fstream file;
    file.open(fileName, ios::in);
    if(file.good()!=true){
        cout << "Nie mozna otworzyc pliku" << endl;
        return;
    }
    randomNumber=Draw(1, 10);
    int noQuestion;
    string trueAnswer;

    vector <string> row;
    string line, data;

    while(!file.eof()){
        row.clear();
        getline(file, line);
        stringstream s(line);

        while (getline(s, data, ';')){
            row.push_back(data);
        }
        noQuestion=stoi(row[0]);
        if(noQuestion == randomNumber){
            question=row[1];
            A=row[2];
            B=row[3];
            C=row[4];
            D=row[5];
            trueAnswer=row[6];
            trueAnswerNo=stoi(trueAnswer);
            break;
        }
    }
    file.close();
    Question::print();
}

void Question::print(){
    Question::lifebuoy();
    cout << question << endl;
    cout << "A. " << A << "\t\t";
    cout << "B. " << B << endl;
    cout << "C. " << C << "\t\t";
    cout << "D. " << D << endl;
    Question::reply();
}

void Question::reply(){
    cin >> answer;

    if(answer=="A" || answer=="a") answerNo=1;
    else if(answer=="B" || answer=="b") answerNo=2;
    else if(answer=="C" || answer=="c") answerNo=3;
    else if(answer=="D" || answer=="d") answerNo=4;
    else if(answer=="50/50" || answer=="50:50"){
        if(fiftyfifty==0){
            fiftyfifty++;
            fiftyFifty();
            return;
        }
        else{
            cout << endl << "Uzyles juz tego kola ratunkowego!" << endl << "Podaj odpowiedz lub skorzystaj z innego kolo ratunkowego." << endl << endl;
            Question::reply();
            return;
        }
    }
    else if(answer=="publicznosc" || answer=="Publicznosc"){
        if(audience==0){
            audience++;
            crowd();
            return;
        }
        else{
            cout << endl << "Uzyles juz tego kola ratunkowego!" << endl << "Podaj odpowiedz lub skorzystaj z innego kolo ratunkowego." << endl << endl;
            Question::reply();
            return;
        }
    }
    else if(answer=="przyjaciel" || answer=="Przyjaciel"){
        if(callFriend==0){
            callFriend++;
            phone();
            return;
        }
        else{
            cout << endl << "Uzyles juz tego kola ratunkowego!" << endl << "Podaj odpowiedz lub skorzystaj z innego kolo ratunkowego." << endl << endl;
            Question::reply();
            return;
        }
    }else{
        cout << endl << "NIEDOZWOLONA ODPOWIEDZ!!! Dostepne opcje: A B C D ";
        if(fiftyfifty==0) cout << "50/50; ";
        if(audience==0) cout << "publicznosc; ";
        if(callFriend==0) cout << "przyjaciel; ";
        cout << "." << endl;
        Question::reply();
        return;
    }
    if(answerNo==trueAnswerNo){
        cout << endl;
        if(trueAnswerNo==1) cout << "A";
        if(trueAnswerNo==2) cout << "B";
        if(trueAnswerNo==3) cout << "C";
        if(trueAnswerNo==4) cout << "D";
        cout << " JEST TO POPRAWNA ODPOWIEDZ!" << endl;
        questionNo++;
        if(questionNo!=16){
        cout << endl << "Aby kontynuowac gre wcisnij \"Enter\"." << endl;
        cin.ignore();
        _getch();
        }else endGame();
    }else{
        cout << endl << "\"" << answer<< "\" JEST TO NIEPOPRAWNA ODPOWIEDZ!" << endl;
        cout << "POPRAWNA ODPOWIEDZ TO ";
        if(trueAnswerNo==1) cout << "A";
        if(trueAnswerNo==2) cout << "B";
        if(trueAnswerNo==3) cout << "C";
        if(trueAnswerNo==4) cout << "D";
        cout << endl << endl;
        endGame();
        questionNo=50;
    }
}

void Question::lifebuoy()
{
    cout << endl << "Dostepne kola ratunkowe:";
    if(fiftyfifty==0){
        cout << endl << "50:50  (wpisz \"50:50\" lub \"50/50\" aby uzyc kolo)";
    }
    if(audience==0){
        cout << endl << "Pytanie do publicznosci (wpisz \"publicznosc\" aby uzyc kolo)";
    }
    if(callFriend==0){
        cout << endl << "Telefon do przyjaciela (wpisz \"przyjaciel\" aby uzyc kolo)";
    }
    else if(fiftyfifty!=0 && audience!=0 && callFriend!=0){
        cout << endl << "Brak";
    }
    cout << endl << endl;
}

void Question::fiftyFifty(){
    int maybeTrue;
    maybeTrue=trueAnswerNo;
    while(trueAnswerNo==maybeTrue){
        maybeTrue=Draw(1, 4);
    }
    if(trueAnswerNo!=1 && maybeTrue!=1) A="";
    if(trueAnswerNo!=2 && maybeTrue!=2) B="";
    if(trueAnswerNo!=3 && maybeTrue!=3) C="";
    if(trueAnswerNo!=4 && maybeTrue!=4) D="";
    Question::print();
}

void Question::crowd()
{
    int max=100, Apro=0, Bpro=0, Cpro=0, Dpro=0;

    if(trueAnswerNo==1){
        Apro=Draw(50, max-50);
        max-=Apro;
        Bpro=Draw(0, max/3);
        max-=Bpro;
        Cpro=Draw(0, max/2);
        max-=Cpro;
        Dpro=max;
    }
    else if(trueAnswerNo==2){
        Bpro=Draw(50, max-50);
        max-=Bpro;
        Apro=Draw(0, max/3);
        max-=Apro;
        Cpro=Draw(0, max/2);
        max-=Cpro;
        Dpro=max;
    }
    else if(trueAnswerNo==3){
        Cpro=Draw(50, max-50);
        max-=Cpro;
        Apro=Draw(0, max/3);
        max-=Apro;
        Bpro=Draw(0, max/2);
        max-=Bpro;
        Dpro=max;
    }
    else if(trueAnswerNo==4){
        Dpro=Draw(50, max-50);
        max-=Dpro;
        Apro=Draw(0, max/3);
        max-=Apro;
        Bpro=Draw(0, max/2);
        max-=Bpro;
        Cpro=max;
    }
    cout << endl << "Odpowiedzi publicznosci:" << endl;
    cout << "A: " << Apro << "%" << endl;
    cout << "B: " << Bpro << "%" << endl;
    cout << "C: " << Cpro << "%" << endl;
    cout << "D: " << Dpro << "%" << endl;
    print();
}

void Question::phone()
{
    int randomAnswer;
    cout << "Przyjaciel za poprawna uznal odpowiedz: ";
    if(questionNo<=5){
        if(trueAnswerNo==1) cout << "A";
        if(trueAnswerNo==2) cout << "B";
        if(trueAnswerNo==3) cout << "C";
        if(trueAnswerNo==4) cout << "D";
    }else{
        randomAnswer=Draw(1, 4);
        if(randomAnswer==1) cout << "A";
        if(randomAnswer==2) cout << "B";
        if(randomAnswer==3) cout << "C";
        if(randomAnswer==4) cout << "D";
    }
    print();
}

void Question::endGame()
{
    if(questionNo==16) cout << "Gratulacje! Udalo Ci sie wygrac milion zlotych! Wydaj te pieniadze na cos sensownego." << endl;
    else if(questionNo>10) cout << "Niestety przegrales, ale udalo Ci sie osiagnac prog gwarantowany 32 000 zlotych." << endl;
    else if(questionNo>5) cout << "Niestety przegrales, ale udalo Ci sie osiagnac prog gwarantowany 1 000 zlotych." << endl;
    else cout << "Niestety przegrywasz i wracasz do domu z pustymi rekami. Nastepnym razem moze wygrasz." << endl;
}
