#include "functions.h"

using namespace std;

int main()
{
    cout << "Witamy w grze Milionerzy, kiedy nacisniesz klawisz \"Enter\" gra sie rozpocznie." << endl << "Zyczymy udanej zabawy!" << endl;
    _getch();
    system("cls");
    Question obj;

    while(obj.questionNo<16){
        switch(obj.questionNo){
        case 1:
            cout << "~Pierwsze pytanie o 100 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\1.csv");
            break;
        case 2:
            system("cls");
            cout << "~Drugie pytanie o 200 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\2.csv");
            break;
        case 3:
            system("cls");
            cout << "~Trzecie pytanie o 300 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\3.csv");
            break;
        case 4:
            system("cls");
            cout << "~Czwarte pytanie o 500 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\4.csv");
            break;
        case 5:
            system("cls");
            cout << "~Piate pytanie o gwarantowane 1000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\5.csv");
            break;
        case 6:
            system("cls");
            cout << "~Szoste pytanie o 2000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\6.csv");
            break;
        case 7:
            system("cls");
            cout << "~Siodme pytanie o 4000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\7.csv");
            break;
        case 8:
            system("cls");
            cout << "~Osme pytanie o 8000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\8.csv");
            break;
        case 9:
            system("cls");
            cout << "~Dziewiate pytanie o 16 000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\9.csv");
            break;
        case 10:
            system("cls");
            cout << "~Dziesiate pytanie o gwarantowane 32 000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\10.csv");
            break;
        case 11:
            system("cls");
            cout << "~Jedenaste pytanie o 64 000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\11.csv");
            break;
        case 12:
            system("cls");
            cout << "~Dwunaste pytanie o 125 000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\12.csv");
            break;
        case 13:
            system("cls");
            cout << "~Trzynaste pytanie o 250 000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\13.csv");
            break;
        case 14:
            system("cls");
            cout << "~Czternaste pytanie o 500 000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\14.csv");
            break;
        case 15:
            system("cls");
            cout << "~Pietnaste pytanie o 1 000 000 zl~" << endl;
            obj.questionData("..\\project_07_35186\\questions\\15.csv");
            break;
        }
    }
    return 0;
}
