#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <conio.h>

using namespace std;

class Question{
private:
    string question, A, B, C, D, answer;
    int randomNumber, answerNo, trueAnswerNo, fiftyfifty=0, audience=0, callFriend=0;

public:
    int questionNo=1;
    void questionData(string fileName);
    void print();
    void reply();
    void endGame();

    void lifebuoy();
    void fiftyFifty();
    void crowd();
    void phone();
};

int draw(const int max);

#endif // FUNCTIONS_H
